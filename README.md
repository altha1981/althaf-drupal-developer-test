## Installation

1. Clone the repo 
2. Import the database using the dump (to make it easy added to the root folder and the file is dnsw_atlas-final.sql)
3. Update database details in settings.php
4. Run composer install
5. Clear cache using Drush

---

## Modules worked on

Below are the custom modules added to list accommodation options in New South Wales by pulling data from ATDW Atlas API.
Module dnsw_atlas_integration is for the API integration. 
Module dnsw_atlas_products is build on Drupal 8 Form API. It returns products that have been retreived on the fly from the ATDW Atlas API endpoint.

1. https://bitbucket.org/altha1981/althaf-drupal-developer-test/src/master/web/modules/custom/dnsw_atlas_integration/
2. https://bitbucket.org/altha1981/althaf-drupal-developer-test/src/master/web/modules/custom/dnsw_atlas_products/

## API endpoint

Default:
https://atlas.atdw-online.com.au/api/atlas/products?cats=ACCOMM&st=NSW&out=json&key=2015201520159

With Area Filter:
https://atlas.atdw-online.com.au/api/atlas/products?cats=ACCOMM&st=NSW&out=json&key=2015201520159&rg=Hunter 

## Coding

1. Site set up using composer and made easy to clone it locally
2. Drupal 8 Form API to build the module dnsw_atlas_products
3. dnsw_atlas_integration makes API endpoint URL and Key configurable
4. Results from Filter options are displayed using Drupal\Core\Ajax\AjaxResponse
5. Drupal Coding standard for modules added is tested using php-codesniffer and command is phpcs --standard=Drupal -- web\modules\custom\custom\
6. UTF-16LE content response is converted to UTF-8 using iconv (https://www.php.net/manual/en/function.iconv.php)

## Configuration

See below the configurations added. 

1. API URL and Key settings to ATDW Atlas API can be updated in the admin section. Use path /admin/config/services/dnsw_atlas_settings
2. Configs are exported and is located under config folder in the root directory

## Hosting

Hosted on https://platform.sh/. platform.sh provides free Dev accounts.

URLS:

https://master-7rqtwti-t55izmohkalry.au.platformsh.site/