<?php

namespace Drupal\dnsw_atlas_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Url;
use GuzzleHttp\Client;

/**
 * Class DnswAtlasIntegration.
 */
class DnswAtlasIntegration {

  const GET = 'GET';
  const POST = 'POST';

  /**
   * The base url.
   *
   * @var string
   */
  private $baseUrl;

  /**
   * The api key value.
   *
   * @var string
   */
  private $apiKey;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  private $client;

  /**
   * Drupal logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Constructs a new DnswAtlasIntegration object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \GuzzleHttp\Client $client
   *   Http Client.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Drupal logger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Client $client, LoggerChannelFactory $logger_factory) {
    $this->configFactory = $config_factory;
    $config = $config_factory->get('dnsw_atlas_integration.settings');
    $this->baseUrl = $config->get('base_url');
    $this->apiKey = $config->get('api_key');
    $this->client = $client;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Call Atlas Api endpoint to get accomodation detail.
   *
   * @param string $accommodation
   *   ACCOMM string.
   * @param string $state
   *   State string.
   * @param string $region
   *   Region string.
   *
   * @return array|bool
   *   Response array.
   */
  public function accommDetailRequest($accommodation, $state, $region) {
    $endpoint = '/products';
    $data = [
      'cats' => $accommodation,
      'st' => $state,
      'out' => 'json',
    ];

    if ($region) {
      $data['rg'] = $region;
    }

    $response = $this->sendAtlasApiRequest($endpoint, $data);
    if (!empty($response)) {
      return $response;
    }

    return FALSE;
  }

  /**
   * Helper function to send request to the Atlas API resource.
   *
   * @param string $endpoint
   *   Endpoint.
   * @param array|string $data
   *   Data send in request.
   * @param string $type
   *   Request type.
   *
   * @return array|bool
   *   Response array.
   */
  private function sendAtlasApiRequest($endpoint, $data, $type = self::GET) {
    $options = [
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'X-Version' => 1,
      ],
    ];

    $url = $this->baseUrl . $endpoint;

    switch ($type) {
      case self::POST:
        $options['body'] = $data;
        break;

      case self::GET:
        $data['key'] = $this->apiKey;
        $url = Url::fromUri($url, ['query' => $data])->toUriString();
        break;
    }

    try {
      $response = $this->client->request($type, $url, $options);
      $response = $response->getBody()->getContents();
      $response = iconv('utf-16le', 'UTF-8', $response);
      $response = json_decode($response, TRUE);
      return (array) $response;
    }
    catch (ClientException $e) {
      $response = $e->getResponse();
      $response = $response->getBody()->getContents();
      $response = iconv('utf-16le', 'UTF-8', $response);
      json_decode($response);
    }
    catch (GuzzleException $e) {
      $response = $e->getMessage();
      $this->loggerFactory->get('dnsw_atlas_integration')->error($response);
    }
    return FALSE;
  }

}
