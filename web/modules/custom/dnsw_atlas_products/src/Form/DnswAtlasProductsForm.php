<?php

namespace Drupal\dnsw_atlas_products\Form;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\dnsw_atlas_integration\DnswAtlasIntegration;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Atlas Products form.
 */
class DnswAtlasProductsForm extends FormBase {

  const ACCOMMODATION = 'ACCOMM';

  const STATE = 'NSW';

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Manager block plugin.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * Atlas api.
   *
   * @var \Drupal\dnsw_atlas_integration\DnswAtlasIntegration
   */
  protected $atlasApi;

  /**
   * Temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Temp store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * DnswAtlasProductsForm constructor.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   Form builder.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   Manager block plugin.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\dnsw_atlas_integration\DnswAtlasIntegration $atlas_api
   *   Atlas api detail.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempStoreFactory
   *   Private tempstore factory.
   */
  public function __construct(
    FormBuilderInterface $form_builder,
    RendererInterface $renderer,
    BlockManagerInterface $block_manager,
    EntityTypeManagerInterface $entity_type_manager,
    DnswAtlasIntegration $atlas_api,
    PrivateTempStoreFactory $tempStoreFactory
  ) {
    $this->formBuilder = $form_builder;
    $this->renderer = $renderer;
    $this->blockManager = $block_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->atlasApi = $atlas_api;
    $this->tempStoreFactory = $tempStoreFactory;
    $this->tempStore = $this->tempStoreFactory
      ->get('region_form_values');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    /** @var \Drupal\Core\Form\FormBuilderInterface $form_builder */
    $form_builder = $container->get('form_builder');
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = $container->get('renderer');
    /* @var \Drupal\Core\Block\BlockManagerInterface $block_manager */
    $block_manager = $container->get('plugin.manager.block');
    /** @var \Drupal\dnsw_atlas_integration\DnswAtlasIntegration $atlas_api */
    $atlas_api = $container->get('dnsw_atlas_integration.api');
    /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $tempStore */
    $tempStoreFactory = $container->get('tempstore.private');
    return new static($form_builder, $renderer, $block_manager, $entity_type_manager, $atlas_api, $tempStoreFactory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dnsw_atlas_products_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $accomm_data = [
      '#products_info' => $this->getAccommodationInfo($region = NULL),
      '#theme' => 'dnsw_atlas_products',
    ];

    $form['#attached']['library'][] = 'dnsw_atlas_products/products';

    $form += ['#prefix' => '<div id="product_list-form-wrapper">'];
    $form += ['#suffix' => '</div>'];

    $form += [
      'select_region' => [
        '#type' => 'select',
        '#title' => $this->t('Show All'),
        '#options' => [
          '' => $this->t('Select All'),
          'Blue Mountains' => $this->t('Blue Mountains'),
          'Central Coast' => $this->t('Central Coast'),
          'Country NSW' => $this->t('Country NSW'),
          'Hunter' => $this->t('Hunter'),
          'Lord Howe Island' => $this->t('Lord Howe Island'),
          'North Coast' => $this->t('North Coast'),
          'Outback NSW' => $this->t('Outback NSW'),
          'Snowy Mountains' => $this->t('Snowy Mountains'),
          'South Coast' => $this->t('South Coast'),
        ],
        '#ajax' => [
          'callback' => '::ajaxChangeRegion',
          'disable-refocus' => FALSE,
          'event' => 'change',
          'wrapper' => 'edit-output',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('loading best results...'),
          ],
        ],
      ],
    ];

    $form += [
      'product_list' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['product-list'],
        ],
        'content' => [
          '#markup' => '<div id="product_list-wrapper">' . render($accomm_data) . '</div>',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Implement submitForm() method.
  }

  /**
   * Helper function for getting details for ACCOMM in NSW.
   *
   * @param string $region
   *   To filter by region.
   *
   * @return array
   *   Associative array which is prepared for rendering.
   */
  private function getAccommodationInfo($region) {
    $accommodation = self::ACCOMMODATION;
    $state = self::STATE;

    $product = [];

    $accomm_results = $this->atlasApi->accommDetailRequest($accommodation, $state, $region);

    $total_results = count($accomm_results['products']);

    if ($total_results > 0) {
      $i = 0;
      foreach ($accomm_results['products'] as $key => $value) {
        $product[$i]['productName'] = $value['productName'];
        $product[$i]['productImage'] = $value['productImage'];
        $product[$i]['city'] = $value['addresses'][0]['city'];
        $product[$i]['area'] = $value['addresses'][0]['area'][0];
        $product[$i]['region'] = $value['addresses'][0]['region'][0];
        $product[$i]['productDescription'] = $value['productDescription'];
        $product[$i]['productName'] = $value['productName'];
        $i++;
      }
      $product['totalRecords'] = $total_results;
    }

    return $product;
  }

  /**
   * Ajax form callback. Data replacement.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax Response object.
   */
  public function ajaxChangeRegion(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $this->tempStore->set('region', 'North Coast');

    $selected_region = $form_state->getValue('select_region');
    $region = ($selected_region != '' ? $selected_region : NULL);

    $accomm_data = [
      '#products_info' => $this->getAccommodationInfo($region),
      '#theme' => 'dnsw_atlas_products',
    ];

    $form = [
      'product_list' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['product-list'],
        ],
        'content' => [
          '#markup' => '<div id="product_list-wrapper">' . render($accomm_data) . '</div>',
        ],
      ],
    ];

    $ajax_response->addCommand(new ReplaceCommand('#product_list-wrapper', $form['product_list']));
    return $ajax_response;
  }

}
